
import { defineConfig } from 'vite'
import cleanPlugin from 'vite-plugin-clean'
import inputs from './vite.inputs.js'
import preact from '@preact/preset-vite'

const output = '../dist/'
const assets = ''
const isWatch = process.argv.includes('--watch')

// https://vitejs.dev/config/
export default defineConfig(({mode}) => ({
  base: './',
  root: './src',

  plugins: [
    cleanPlugin({
      targetFiles: [output]
    }),
    preact()
  ],
  build: {
    minify: mode === 'development' || isWatch === true ? false : 'terser',
    emptyOutDir: true,
    outDir: output,
    sourcemap: false,
    cssCodeSplit: true,
    rollupOptions: {
      input: inputs,
      output: {
        chunkFileNames: `${assets}js/[name].min.js`, // these lines and uncomment the bottom ones
        entryFileNames: `${assets}js/[name].min.js`, 
        assetFileNames: assetInfo => {
          const info = assetInfo.name.split('.')
          const extType = info[info.length - 1]
          if (/\.(png|jpe?g|gif|svg|webp|webm|mp3)$/.test(assetInfo.name)) {
            return `${assets}img/${extType}/[name].${extType}`
          }
          if (/\.(css)$/.test(assetInfo.name)) {
            return `${assets}css/[name].min.${extType}`
          }
          if (/\.(woff|woff2|eot|ttf|otf)$/.test(assetInfo.name)) {
            return `${assets}fonts/[name].min.${extType}`
          }
          return `[name].${extType}`
        },
      },
    }
  },
  server: {
    open: 'test.html',
    useFullPath: true
  },
}))