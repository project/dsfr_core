import { resolve } from 'path'

const preact = 'src/preact/'
const style = 'src/style/'

const inputs = {

  color_selector: resolve(__dirname, style+'color_selector.css'),
  icons_list: resolve(__dirname, preact+'icons/dsfr-icons-container.jsx'),
  pictograms_list: resolve(__dirname, preact+'pictograms/dsfr-pictograms-container.jsx')
}

export default inputs