export const validateNameFilter = (value) => {
  return value.toLowerCase().replace(/[^a-z0-9-]/g, '');
};