import { useState, useEffect } from 'preact/hooks';
import pictograms from '../../json/pictograms-list.json';
import pictogramsSettings from '../../json/pictograms-settings.json';
import settings from '../../json/settings.json';
import { PictogramsList } from './pictograms-list-controller.jsx';

export const App = ({ pathDsfr, initialSearchTerm, language }) => {

  const [pictogramsData, setPictogramsData] = useState(null);

  useEffect(() => {
    const mockData = pictograms;
    setPictogramsData(mockData);
  }, []);

  const ln = language == 'fr' ? 'fr' : 'en'; 
  const labels = { ...pictogramsSettings.languages[ln] , ...settings.languages[ln] };

  if (!pictogramsData) return <div>{labels.load}</div>;

  const listProps = {
    pathDsfr,
    pictogramsData,
    initialSearchTerm, 
    labels
  }

  return <PictogramsList {...listProps} />;
};