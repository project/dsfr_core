import { useRef, useEffect, useState } from 'preact/hooks';
import { PictogramSvg } from './pictogram-svg.jsx';
import { renderToString } from '../commons/render-component.jsx';

export const CodeBlock = ({ pictogramsPath, type, name, labels }) => {

  const preRef = useRef(null);
  const [buttonText, setButtonText] = useState(labels.code);
  const [pictogramHtml, setPictogramHtml] = useState('');

  useEffect(() => {
    const html = renderToString(<PictogramSvg pictogramsPath={pictogramsPath} type={type} name={name} />);
    setPictogramHtml(html);
  }, []);

  const copyToClipboard = async () => {
    
    if (preRef.current) {
      const code = preRef.current.textContent;
      try {
        await navigator.clipboard.writeText(code);
        setButtonText(labels.copy);
        setTimeout(() => setButtonText(labels.code), 2000);
      } catch (err) {
        console.error(labels.console, err);
        setButtonText(labels.error);
      }
    }
  };

  return (
    <>
      <div class="fr-code fr-mt-3w">
        <pre ref={preRef} class="language-twig" tabindex="0">
          <code class="language-twig">{pictogramHtml}</code>
        </pre>
        <button class="fr-btn copy-code-button" onClick={copyToClipboard}>{buttonText}</button>
      </div>
    </>
  );
};