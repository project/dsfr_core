import { useState, useEffect, useCallback, useMemo } from 'preact/hooks';
import { Form } from '../commons/form.jsx';
import { PictogramButton } from './pictogram-button.jsx';
import { PictogramModal } from './pictogram-modal.jsx';
import { Pagination } from '../commons/pagination.jsx';
import { validateNameFilter } from '../commons/valid-name.jsx';

export const PictogramsList = ({ pathDsfr, pictogramsData, initialSearchTerm, labels }) => {

  const [nameFilter, setNameFilter] = useState(validateNameFilter( initialSearchTerm ));
  const [typeFilter, setTypeFilter] = useState('all');

  const [currentPage, setCurrentPage] = useState(1);
  const pictogramsPerPage = 40;

  // ------------------------------------------------------------------------------ //
  // Listing & filter controller

  const handleNameFilterChange = useCallback((e) => {
    const newValue = validateNameFilter(e.target.value);
    setNameFilter(newValue);
  }, []);

  const handleTypeFilterChange = useCallback((e) => {
    setTypeFilter(e.target.value);
    setCurrentPage(1);
  }, []);

  const allPictograms = useMemo(() => {
    return Object.entries(pictogramsData)
      .flatMap(([type, pictograms]) => 
        pictograms.map(pictogram => ({ name: pictogram, type }))
      )
      .sort((a, b) => a.name.localeCompare(b.name));
  }, [pictogramsData]);

  const types = useMemo(() => {
    return ['all', ...Object.keys(pictogramsData)];
  }, [pictogramsData]);

  const filteredPictograms = useMemo(() => {
    return allPictograms.filter(pictogram => 
      pictogram.name.toLowerCase().includes(nameFilter.toLowerCase()) &&
      (typeFilter === 'all' || pictogram.type === typeFilter)
    );
  }, [allPictograms, nameFilter, typeFilter]);

  const formProps = {
    nameFilter,
    handleNameFilterChange, 
    typeFilter,
    handleTypeFilterChange,
    types,
    labels
  }

  // ------------------------------------------------------------------------------ //
  // Pages controller
  
  useEffect(() => {
    setCurrentPage(1);
  }, [nameFilter, typeFilter]);

  const totalPages = Math.max(1, Math.ceil(filteredPictograms.length / pictogramsPerPage));
  const hasResults = filteredPictograms.length > 0;

  const currentPictograms = useMemo(() => {
    const startIndex = (currentPage - 1) * pictogramsPerPage;
    return filteredPictograms.slice(startIndex, startIndex + pictogramsPerPage);
  }, [filteredPictograms, currentPage]);


  const paginationProps = {
    nameFilter,
    typeFilter,
    currentPage,
    setCurrentPage,
    hasResults,
    totalPages,
    labels
  };

  const pictogramsPath = `${pathDsfr}/dist/dsfr/artwork/pictograms`;

  // ------------------------------------------------------------------------------ //

  return (
    <>
      <Form {...formProps} />

      {/* Listing (with pagination) */}
      <p class="fr-mt-3v">
        <strong>{filteredPictograms.length} {labels.item}(s)</strong> | {labels.see}:&nbsp; 
        <a class="fr-link" href="https://www.systeme-de-design.gouv.fr/fondamentaux/pictogramme" target="_blank" rel="noopener external">{labels.official}</a>
      </p>
      <div class="fr-grid-row fr-mt-9v">
        {currentPictograms.map(pictogram => (
          <div key={pictogram.name} class="fr-col-12 fr-col-lg-3 fr-mb-3v">
            <PictogramButton pictogramsPath={pictogramsPath} type={pictogram.type} name={pictogram.name} />
            <PictogramModal pictogramsPath={pictogramsPath} type={pictogram.type} name={pictogram.name} labels={labels} /> 
          </div>
        ))}
      </div>
      <div>
        <Pagination {...paginationProps} />      
      </div>
    </>
  );
};