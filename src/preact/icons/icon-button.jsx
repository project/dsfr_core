import { IconSpan } from './icon-span.jsx';

export const IconButton = ({ name }) => (
  <button 
    class="fr-btn fr-btn--tertiary-no-outline"
    data-fr-opened="false" 
    aria-controls={`fr-modal-${name}`}
  >
    <IconSpan name={name} option=" fr-icon--lg" />
    <span class="fr-text--sm fr-ml-1v drupal-icon-item">{name}</span>
  </button>
)