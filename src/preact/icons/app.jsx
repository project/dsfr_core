import { useState, useEffect } from 'preact/hooks';
import icons from '../../json/icons-list.json';
import iconsSettings from '../../json/icons-settings.json';
import settings from '../../json/settings.json';
import { IconsList } from './icons-list-controller.jsx';

export const App = ({ initialSearchTerm, language }) => {

  const [iconsData, setIconsData] = useState(null);

  useEffect(() => {
    const mockData = icons;
    setIconsData(mockData);
  }, []);

  const ln = language == 'fr' ? 'fr' : 'en'; 
  const labels = { ...iconsSettings.languages[ln] , ...settings.languages[ln] };

  if (!iconsData) return <div>{labels.load}</div>;

  const listProps = {
    iconsData,
    initialSearchTerm, 
    labels
  }

  return <IconsList {...listProps} />;
};