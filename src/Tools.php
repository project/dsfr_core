<?php

namespace Drupal\dsfr_core;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Database;
use Drupal\Core\Url;

/**
 * Provides DSFR-themed tools for all child modules in the DSFR environment
 */
class Tools extends ControllerBase {

  /**
   * Checks if the DSFR theme is present or installed.
   *
   * @return array
   *   An array containing:
   *   - 'current': The current theme.
   *   - 'themes': List of all available themes.
   *   - 'missing': Boolean indicating if DSFR theme is missing.
   */
  public function checkTheme(): array {

    // Get current theme
    $theme_current = \Drupal::config('system.theme')->get('default');
    // Get all themes
    $themes = \Drupal::service('extension.list.theme')->getList(); 

    return [
      'current' => $theme_current,
      'themes' => $themes,
      // Check if DSFR Drupal Theme exists
      'missing' => ( $theme_current != 'dsfr' && !array_key_exists('dsfr', $themes) ) ? true : false
    ];
  }

  /**
   * Checks if the DSFR theme is activated.
   *
   * @param string $theme
   *   The theme name to check, defaults to 'dsfr'.
   *
   * @return string
   *   The name of the active theme.
   */
  public function checkThemeActivated(string $theme = 'dsfr'): string {

    $theme_manager = \Drupal::service('theme.manager');
    $active_theme = $theme_manager->getActiveTheme();

    return $active_theme->getName($theme);
  }

  /**
   * Checks if the current theme has DSFR Regions.
   *
   * @return array
   *   An array containing:
   *   - 'regions': List of all regions in the current theme.
   *   - 'not_found': Message if DSFR regions are not found.
   */
  public function checkRegions(): array {

    $theme = $this->checkTheme();
    $theme_current = $theme['current'];

    $message_region_not_found = $this->t('One or more associated DSFR regions(*) not found for the default theme');
    $message_region_not_found.= ' ('. $theme_current .')';

    return [
      'regions' => system_region_list($theme_current, $show = REGIONS_ALL),
      'not_found' => $message_region_not_found
    ];
  }

  /**
   * Checks if a specific module is installed and retrieves its routes.
   *
   * @param string $module_name
   *   The name of the module to check.
   * @param array $routes
   *   An array of route names to retrieve paths for.
   *
   * @return array
   *   An array containing:
   *   - 'check': Boolean indicating if the module exists.
   *   - 'links': Array of paths for the specified routes.
   */
  public function checkModules( string $module_name, array $routes = [] ): array {

    $check = \Drupal::moduleHandler()->moduleExists($module_name);
    $links = [];

    if( $check == true && count($routes) > 0 ) {

      foreach( $routes as $key => $route ) {

        $links[$key] = $this->path( $route ); 
      }
    }

    return [
      'check' => $check,
      'links' => $links
    ];
  }

  /**
   * Retrieves the major version of Drupal,
   * to check if the code is compatible, 
   * like Ckeditor5 for example.
   *
   * @return string
   *   The major version number of Drupal as a string.
   */
  public function checkDrupalVersion(): string {

    $drupal_version = \Drupal::VERSION;
    $version = explode( '.', $drupal_version );

    return $version[0];
  }

  /** --------------------------------------------------------------------------------- */
  // Database

  /**
   * Checks if a database table exists.
   *
   * @param string|null $table_name
   *   The name of the table to check.
   *
   * @return bool
   *   TRUE if the table exists, FALSE otherwise.
   */
  public function checkTable( ?string $table_name = NULL ): bool {

    if( $table_name != NULL ) {

      $db = Database::getConnection();
      return $db->schema()->tableExists( $table_name );
    }
  }

  /**
   * Creates a new database table.
   *
   * @param string|null $table_name
   *   The name of the table to create.
   * @param array $settings
   *   An array of table settings and field definitions.
   */
  public function createTable( ?string $table_name = NULL, array $settings = [] ): void {

    if( $table_name != NULL && count($settings) > 0 ) {

      if( $this->checkTable( $table_name) == false ) {

        $db = Database::getConnection();  
        $db->schema()->createTable( $table_name, $settings );  
      }
    }
  }

  /**
   * Retrieves the column information for a database table.
   *
   * @param string|null $table_name
   *   The name of the table to query.
   *
   * @return array|null
   *   An array of column information, or NULL if the table doesn't exist.
   */
  public function columnsTable( ?string $table_name = NULL ): ?array {

    if( $table_name != NULL ) {

      $db = Database::getConnection();  
      $query = $db->query("SHOW COLUMNS FROM $table_name");
      return $query->fetchAll();
    }
  }

  /**
   * Performs a SELECT query on a database table.
   *
   * @param string|null $table_name
   *   The name of the table to query.
   * @param string $what
   *   The columns to select, defaults to '*'.
   * @param string $where
   *   The WHERE clause of the query.
   *
   * @return array|null
   *   An array of query results, or NULL if the table doesn't exist.
   */
  public function selectTable(
    ?string $table_name = NULL, 
    string $what = '*', 
    string $where = ''
  ): ?array {

    if( $table_name != NULL ) {

      $db = Database::getConnection();  
      $query = $db->query("SELECT $what FROM $table_name $where");
      return $query->fetchAll();
    }
  }

  /**
   * Inserts a new record into a database table.
   *
   * @param string|null $table_name
   *   The name of the table to insert into.
   * @param array $fields
   *   An associative array of field names and values to insert.
   */
  public function insertTable( ?string $table_name = NULL, array $fields = [] ): void {

    if( $table_name != NULL && count($fields) > 0 ) {

      if( $this->checkTable( $table_name )) {

        $db = Database::getConnection();  
        $db->insert( $table_name )
        ->fields( $fields )
        ->execute();
      }
    }
  }

  /**
   * Updates records in a database table.
   *
   * @param string|null $table_name
   *   The name of the table to update.
   * @param array $fields
   *   An associative array of field names and values to update.
   * @param string|null $field
   *   The field name to use in the condition.
   * @param mixed $value
   *   The value to use in the condition.
   * @param string $equal
   *   The operator to use in the condition, defaults to '='.
   */
  public function updateTable(
    ?string $table_name = NULL, 
    array $fields = [], 
    ?string $field = NULL, 
    $value = NULL, 
    string $equal = '='
  ): void {

    if( $table_name != NULL && count($fields) > 0 && $field != NULL && $value != NULL ) {

      if( $this->checkTable( $table_name )) {

        $db = Database::getConnection();  
        $db->update( $table_name )
        ->fields( $fields )
        ->condition( $field, $value, $equal )
        ->execute();
      }
    }
  }

  /**
   * Deletes records from a database table.
   *
   * @param string|null $table_name
   *   The name of the table to delete from.
   * @param bool $permission
   *   A flag to confirm deletion permission.
   * @param string|null $field
   *   The field name to use in the condition.
   * @param mixed $value
   *   The value to use in the condition.
   * @param string $equal
   *   The operator to use in the condition, defaults to '='.
   *
   * @warning Use with caution as this operation cannot be undone.
   */
  public function deleteTable(
    ?string $table_name = NULL, 
    bool $permission = false, 
    ?string $field = NULL, 
    $value = NULL, 
    string $equal = '='
  ): void {

    if( $table_name != NULL && $permission == true ) {

      if( $this->checkTable( $table_name )) {

        $db = Database::getConnection();  
  
        if( $field != NULL && $value != NULL ) {
  
          $db->delete( $table_name )
          ->condition( $field, $value, $equal )
          ->execute();
  
        } else {
  
          $db->delete( $table_name )
          ->execute(); 
        }
      }
    }
  }

  /**
   * Drops a database table.
   *
   * @param string|null $table_name
   *   The name of the table to drop.
   * @param bool $permission
   *   A flag to confirm drop permission.
   *
   * @warning USE WITH EXTREME CAUTION. This operation cannot be undone.
   */
  public function dropTable( ?string $table_name = NULL , bool $permission = false ): void {

    if( $table_name != NULL && $permission == true ) {

      if( $this->checkTable( $table_name )) {

        $db = Database::getConnection();  
        $db->schema()->dropTable( $table_name );
      }
    }
  }

  /** --------------------------------------------------------------------------------- */
  // Others

  /**
   * Displays a pre-formatted Drupal message that supports HTML.
   *
   * @param string $get_msg
   *   The message to display.
   * @param string $type
   *   The type of message (success, warning, or error).
   */
  public function msg( string $get_msg, string $type = 'success' ): void {

    $msg = $this->t($get_msg);

    switch($type) {

      case 'warning':
        \Drupal::messenger()->addWarning($msg); 
        break;

      case 'error':
        \Drupal::messenger()->addError($msg); 
        break;

      default:
        \Drupal::messenger()->addStatus($msg); 
    }
  }

  /**
   * Generates a URL path from a route.
   *
   * @param string $route
   *   The route name.
   * @param array $params
   *   An array of parameters for the route.
   *
   * @return string
   *   The generated URL path.
   */
  public function path( string $route, array $params = [] ): string {

    $url = Url::fromRoute( $route, $params );
    return $url->toString();
  }

  /**
   * Retrieves the default timezone setting.
   *
   * @return string
   *   The default timezone setting.
   */
  public function timeZone(): string {
  
    $config_factory = \Drupal::service('config.factory');
    $config = $config_factory->get('system.date');
    return $config->get('timezone')['default'];
  }
}