<?php

namespace Drupal\dsfr_core;

use Drupal\Core\Controller\ControllerBase;

/**
 * Provides storage of fields for different modules,  
 * such as dsfr_paragraph and dsfr_block
 */
class FieldStorage extends ControllerBase {

  /**
   * See: https://www.drupal.org/docs/drupal-apis/entity-api/fieldtypes-fieldwidgets-and-fieldformatters
   */
  public function fieldData(): array {

    $fields = [
    
      'block' => [
        'label' => $this->t('bloc'),
        'type' => 'block_field',
        'module' => 'block_field',
        'settings' => [
          'selection' => 'blocks',
          'selection_settings' => ['plugin_ids' => []],
        ],
      ],

      'body' => [  
        'label' => $this->t('Body'),
        'type' => 'text_textarea_with_summary',
        'view' => 'text_default',
        'required' => true
      ],

      'ckeditor5' => [
        'label' => $this->t('Text'),
        'type' => 'text_long'
      ],

      'color' => [
        'label' => $this->t('Color'),
        'type' => 'style_selector_css_class',
        'display' => NULL, //'style_selector_compact_widget',
        'style_type' => 'css_class',
        'view' => 'list_key',
        'module' => 'options',
        'settings' => [
          'allowed_values' => $this->colorsSimple(),
        ],
      ],

      'media_image' => [
        'label' => $this->t('Media image'),
        'type' => 'entity_reference',
        'display' => 'media_library_widget',
        'view' => 'entity_reference_entity_view',
        'module' => 'core',
        'settings' => [
          'target_type' => 'media'
        ],
      ],

      'message' => [
        'label' => $this->t('Message'),
        'type' => 'text_long',
        'view' => 'text_default',
        'required' => true,
        'settings' => [
          'allowed_formats' => [
            'restricted_html_dsfr'
          ]
        ]
      ],

      'text' => [
        'label' => $this->t('Text'),
        'type' => 'string_long',
      ],  
    ];

    // ------------------------------------------------------------------------------------------------------------------------- //
    // Input string
  
    $simple = ['author', 'label', 'legend', 'placeholder', 'title', 'tooltip']; 
    $simple_asso = $this->associativeArray($simple);
    $simple_asso['id']['label'] = 'Custom ID';
    foreach ( $simple_asso as $key => $row ) $fields[$key] = ['label' => $this->t($row['label']), 'type' => 'string'];

    // ------------------------------------------------------------------------------------------------------------------------- //
    // Checkbox
  
    $check = ['compact', 'disabled', 'hidden', 'show_color', 'show_icon'];
    $check_asso = $this->associativeArray($check);
    $check_asso['arrow_left']['label'] = 'Arrow icon before label';
    $check_asso['close']['label'] = 'Block can be closed by the user';
    $check_asso['large']['label'] = 'Large size';
    $check_asso['small']['label'] = 'Small size';
    $check_asso['plus']['label'] = 'France-connect plus';
    $check_asso['compact']['desc'] = 'Compact mode reduces internal margins and only the title is displayed (not bold).';

    foreach ( $check_asso as $key => $row ) {
      $fields[$key] = [
        'label' => $this->t($row['label']),
        'type' => 'boolean'
      ];
      if( array_key_exists('desc', $row) ) $fields[$key]['description'] = $this->t($row['desc']);
    } 

    // ------------------------------------------------------------------------------------------------------------------------- //
    // Link

    $fields['link'] = $fields['details'] = [ 'type' => 'link', 'module' => 'link' ];
    $fields['link']['label'] = $this->t('Link');
    $fields['details']['label'] = $this->t('Details');
    $fields['details']['cardinality'] = -1;
    
    // ------------------------------------------------------------------------------------------------------------------------- //
    // List string

    $list = ['arrow_icon', 'badge_type', 'button_type', 'code_type', 'font_size', 'icon_position', 'margin_bottom', 'margin_top', 'size', 'tag_type', 'title_type', 'title_style'];
    $list_asso = $this->associativeArray($list);
    $list_asso['alert']['label'] = 'Alert type';

    $alert_options = [
      'info' => $this->t('Information'),
      'success' => $this->t('Success'),
      'warning' => $this->t('Warning'),
      'error' => $this->t('Error'),
    ];

    foreach ( $list_asso as $key => $row ) {

      $allowed = [];
      switch( $key ) {

        case 'alert': $allowed = $alert_options; break;

        case 'arrow_icon': $allowed = [
          'l' => $this->t('Arrow icon before label'),
          'r' => $this->t('Arrow icon after label')
        ];          
        break;

        case 'badge_type': 
          $allowed = array_merge( ['none' => $this->t('None')] , $alert_options );
          $allowed = array_merge( $allowed, [ 'new' => $this->t('News') ] );          
          break;

        case 'button_type': $allowed = [ 
          'none' => $this->t('Primary'),
          'secondary' => $this->t('Secondary'),
          'tertiary' => $this->t('Tertiary'),
          'tertiary-no-outline' => $this->t('Tertiary no outline')
          ];
          break;

        case 'code_type': $allowed = [
            'apacheconf' => 'Apache conf.',
            'bash' => 'Bash',
            'basic' => 'Basic',
            // 'c' => 'C',
            // 'cpp' => 'C++',
            // 'csharp' => 'C Sharp',
            'css' => 'CSS',
            'clike' => 'C-like',
            'csv' => 'CSV',
            'docker' => 'Docker',
            'git' => 'Git',
            'graphql' => 'GraphQL',
            'markup' => 'HTML',
            'java' => 'Java',
            'javascript' => 'Javascript',
            'json' => 'Json',
            'jsx' => 'Jsx',
            'less' => 'Less',
            'markdown' => 'Markdown',
            'nginx' => 'Nginx',
            'pascal' => 'Pascal',
            'php' => 'PHP',
            'python' => 'Python',
            'tsx' => 'Tsx',
            'sass' => 'SASS',
            'scss' => 'SCSS',
            'sql' => 'SQL',
            'twig' => 'Twig',
            'typescript' => 'Typescript',
            'visual-basic' => 'Visual Basic',
            'xml-doc' => 'XML',
            'yaml' => 'YML',
          ];
          break;

        case 'font_size': $allowed = [ $this->t('By default'), $this->t('Small'), $this->t('Large') ];
          break;

        case 'icon_position': $allowed = [
            'l' => $this->t('Left'),
            'c' => $this->t('Center'),
            'r' => $this->t('Right')
          ];          
          break;

        case 'size': $allowed = [ $this->t('By default'), $this->t('Small'), $this->t('Large') ];
          break;

        case 'title_type':
        case 'title_style': $allowed = [ $this->t('By default'), 'H1', 'H2', 'H3', 'H4', 'H5', 'H6' ]; break;

        case 'tag_type': $allowed = [ $this->t('By default'), $this->t('Can be selected'), $this->t('Can be closed') ];   
          $fields[$key]['desc'] = 'If a value other than the default is selected, the tag will automatically become a button (losing its link property).';
          break;     

        default: $allowed = $this->margin();
      }

      $fields[$key] = [
        'label' => $this->t($row['label']),
        'type' => 'list_string',
        'display' => NULL,
        'view' => 'list_key',
        'module' => 'options',
        'settings' => [ 'allowed_values' => $allowed ],
      ];

    } 
    // ------------------------------------------------------------------------------------------------------------------------- //
    // Double element for PARAGRAPHS

    $double_para = ['accordion', 'badge', 'button', 'card', 'tab', 'tag', 'tile', 'toggle'];
    $double_para_asso = $this->associativeArray($double_para);

    foreach ( $double_para_asso as $key => $row ) {
  
      $fields[$key] = [
        'label' => $this->t($row['label']),
        'type' => 'entity_reference_revisions',
        'entity_type' => 'paragraph',
        'display' => NULL,
        'view' => NULL,
      ];
      $fields[$key.'s'] = [
        'label' => $this->t($row['label'].'s'),
        'type' => 'entity_reference_revisions',
        'entity_type' => 'paragraph',
        'display' => 'paragraphs',
        'view' => NULL,
        'settings' => ['target_type' => 'paragraph'],
        'cardinality' => -1
      ];
    }
    // ------------------------------------------------------------------------------------------------------------------------- //

    ksort($fields);
    return $fields;
  }

  /**
   * See:https://www.systeme-de-design.gouv.fr/elements-d-interface/fondamentaux-techniques/espacement
   * Provides possible margins in style classes
   * Commented code refers to old values 
   */
  public function margin(): array {

    return [
      //'0' => '0 (0px)',
      '0-5v' => '0,5v (2px)',
      '1v' => '1v (4px)',
      '1-5v' => '1,5v (6px)',
      '2v' => '2v (8px)',
      //'1w' => '1w (8px)',
      '3v' => '3v (12px)',
      //'2w' => '2w (16px)',
      '4v' => '4v (16px)',
      '5v' => '5v (20px)',
      '6v' => '6v (24px)',
      //'3w' => '3w (24px)',
      //'4w' => '4W (32px)',
      //'5w' => '5w (40px)',
      //'6w' => '6w (48px)',
      //'7w' => '7w (56px)',
      //'8w' => '8w (64px)',
      //'9w' => '9w (72px)',
      //'12w' => '12w (96px)',
      //'15w' => '15w (120px)',
      '7v' => '7v (28px)',
      '8v' => '8v (32px)',
      '9v' => '9v (36px)',
      '10v' => '10v (40px)',
      '11v' => '11v (44px)',
      '12v' => '12v (48px)',
      '13v' => '13v (52px)',
      '14v' => '14v (56px)',
      '15v' => '15v (60px)',
      '16v' => '16v (64px)',
      '17v' => '17v (68px)',
      '18v' => '18v (72px)',
      '19v' => '19v (76px)',
      '20v' => '20v (80px)',
      '21v' => '21v (84px)',
      '22v' => '22v (88px)',
      '23v' => '23v (92px)',
      '24v' => '24v (96px)',
      '25v' => '25v (100px)',
      '26v' => '26v (104px)',
      '27v' => '27v (108px)',
      '28v' => '28v (112px)',
      '29v' => '29v (116px)',
      '30v' => '30v (120px)',
      '31v' => '31v (124px)',
      '32v' => '32v (128px)',
    ];
  }

  /**
   * See: https://www.drupal.org/docs/contributed-modules/style-selector/style-selector-widgets
   * Provides the main colors available
   */
  public function colors(): array {

    // 

    return [
      'color-default' => [
        'label' => $this->t('Color by default'),
        'color' => '#6a6af4'
      ],   
      'blue-ecume' => [
        'label' => $this->t('Blue Ecume'),
        'color' => '#465f9d'
      ],  
      'blue-cumulus' => [
        'label' => $this->t('Blue Cumulus'),
        'color' => '#417dc4'
      ],
      'green-archipel' => [
        'label' => $this->t('Green Archipel'),
        'color' => '#009099'
      ], 
      'green-menthe' => [
        'label' => $this->t('Green Menthe'),
        'color' => '#009081'
      ],  
      'green-emeraude' => [
        'label' => $this->t('Green Emeraude'),
        'color' => '#00a95f'
      ],  
      'green-bourgeon' => [
        'label' => $this->t('Green Bourgeon'),
        'color' => '#68a532'
      ],  
      'green-tilleul-verveine' => [
        'label' => $this->t('Green Tilleul Verveine'),
        'color' => '#b7a73f'
      ],    
      'yellow-tournesol' => [
        'label' => $this->t('Yellow Tournesol'),
        'color' => '#c8aa39'
      ],  
      'yellow-moutarde' => [
        'label' => $this->t('Yellow Moutarde'),
        'color' => '#c3992a'
      ],
      'brown-cafe-creme' => [
        'label' => $this->t('Brown Cafe Creme'),
        'color' => '#d1b781'
      ],  
      'brown-caramel' => [
        'label' => $this->t('Brown Caramel'),
        'color' => '#c08c65'
      ],  
      'brown-opera' => [
        'label' => $this->t('Brown Opera'),
        'color' => '#bd987a'
      ],
      'pink-macaron' => [
        'label' => $this->t('Pink Macaron'),
        'color' => '#e18b76'
      ],
      'pink-tuile' => [
        'label' => $this->t('Pink Tuile'),
        'color' => '#ce614a'
      ],  
      'purple-glycine' => [
        'label' => $this->t('Purple Glycine'),
        'color' => '#a558a0'
      ],    
      'beige-gris-galet' => [
        'label' => $this->t('Beige Gris Galet'),
        'color' => '#aea397'
      ],
    ];
  }

  /**
   * Simplifies the complete color table by associating the color identifier with its label, 
   * excluding its hexadecimal code
   */
  public function colorsSimple(): array {

    $colors = $this->colors();
    $color_simple = [];
    foreach( $colors as $key => $row ) { $color_simple[$key] = $row['label']; }
    return $color_simple;
  }

  /**
   * Transforms array values into a key associated with a label provided by the value
   */
  public function associativeArray( array $simple_array ): array {

    foreach( $simple_array as $value ) $associativeArray[$value] = ['label' => str_replace('_', ' ', ucfirst($value))];
    return $associativeArray;
  }
}