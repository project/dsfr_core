<?php

namespace Drupal\dsfr_core\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;

class FieldsController extends ControllerBase {

  /**
   * Displays field data for a given entity type.
   *
   * @param string $slug
   *   The entity type slug.
   *
   * @return array|null
   *   A render array containing the field data markup, or null if access is denied.
   */
  public function fieldsData( string $slug = 'block_content' ): ?array {

    // If the current user has the permission
    if (!$this->currentUser()->hasPermission('administer dsfr_core settings')) {
      return null;
    }

    return [
      '#type' => 'markup',
      '#markup' => $this->dsfrCoreFieldManage()->checkFieldsStorage($slug),
    ];
  }

  /**
   * Checks access for the fieldsData route.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user account to check access for.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access( AccountInterface $account ): AccessResult {
    return AccessResult::allowedIfHasPermission($account, 'administer dsfr_core settings');
  }

  /**
   * Gets the dsfr_core.fieldManage service.
   *
   * @return \Drupal\dsfr_core\FieldManage
   *   The dsfr_core.fieldManage service.
   */
  protected function dsfrCoreFieldManage() {
    return \Drupal::service('dsfr_core.fieldManage');
  }
}