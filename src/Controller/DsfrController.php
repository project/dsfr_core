<?php

namespace Drupal\dsfr_core\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;

/**
 * Provides DSFR settings and information pages
 */
class DsfrController extends ControllerBase {

  /**
   * Displays the settings modules page.
   *
   * @return array|null
   *   A render array for the settings page, or null if access is denied.
   */
  public function settingsModules(): ?array {

    // If the current user has the permission
    if (!\Drupal::currentUser()->hasPermission('administer dsfr_core settings')) {
      return null;
    }

    $tools = \Drupal::service('dsfr_core.tools');

    // ------------------------------------------------------------------------------------------------------------ //
    // This module
    
    $dsfr['advanced']['fields']['link'] = $tools->path( 'dsfr_core.fieldmanage' );

    // ------------------------------------------------------------------------------------------------------------ //
    // Get themes info
    
    $theming = $tools->checkTheme();
    $dsfr['theme']['check'] = false;

    if( $theming['missing'] == true ) { 
      
      $tools->msg($this->t('The DSFR theme does not seem to be present in your theme list.'), 'warning');
      
    } else {

      if ( $tools->checkThemeActivated() == true ) {

        $dsfr['theme']['check'] = true;
        $dsfr['theme']['link'] = $tools->path( 'system.theme_settings_theme', ['theme' => 'dsfr'] );
        
      } else {

        $tools->msg($this->t('The theme is present but not active'), 'warning');
      }
    }
    
    // ------------------------------------------------------------------------------------------------------------ //
    // Get modules info

    $dsfr['core'] = [
        'start' => $tools->path('dsfr_core.getstarted'),
        'icons' => $tools->path('dsfr_core.icons'),
        'pictograms' => $tools->path('dsfr_core.pictograms')
      ];

    $dsfr['menu'] = $tools->checkModules('dsfr_menu', 
      [
        'import' => 'dsfr_menu.import'
      ]);

    $dsfr['paragraph'] = $tools->checkModules('dsfr_paragraph', 
      [ 
        'node_type' => 'dsfr_paragraph.manage'
      ]);

    $dsfr['twig'] = $tools->checkModules('dsfr_twig_components', 
      [ 
        'demo' => 'dsfr_twig_components.demo'
      ]);

    return array(
      '#theme' => 'dsfr_settings',
      '#dsfr' => $dsfr,
    );
  }

  /**
   * Redirects to the DSFR theme settings page if the theme is present.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse|null
   *   A redirect response if the theme is present, null otherwise.
   */
  public function theme(): ?RedirectResponse {

    $tools = \Drupal::service('dsfr_core.tools');
    $theming = $tools->checkTheme();

    if ($theming['missing'] == false) { 
      $url = $tools->path('system.theme_settings_theme', ['theme' => 'dsfr']);
      return new RedirectResponse($url);
    }

    return null;
  }

  /**
   * Displays the "Get Started" page.
   *
   * @return array
   *   A render array for the "Get Started" page.
   */
  public function getStarted(): array {

    $tools = \Drupal::service('dsfr_core.tools');
    $theming = $tools->checkTheme();

    if ($theming['missing'] == false) { 
      return ['#theme' => 'dsfr_get_started'];
    } else {
      return ['#markup' => $this->t('DSFR Theme is missing')];
    }
  }

  /**
   * Checks access for the DSFR settings pages.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user account to check access for.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access( AccountInterface $account ): AccessResult {
    return AccessResult::allowedIfHasPermission($account, 'administer dsfr_core settings');
  }
}

?>