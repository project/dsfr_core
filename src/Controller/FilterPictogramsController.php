<?php

namespace Drupal\dsfr_core\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\dsfr_twig_components\Twig\DsfrComponents;
use Drupal\dsfr_twig_components\Twig\Resources;

class FilterPictogramsController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function content( $slug ) {

    return [
      '#theme'=> '_pictograms',
      '#dsfr_path' => base_path() . \Drupal::theme()->getActiveTheme()->getPath(),
      '#slug' => $slug,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function generate() {

    $result = Resources::pictogramsJson('/../../temp/');
    
    return [ '#markup' => DsfrComponents::alert(
      [
      'text' => $result[0], 
      'type' => $result[1],
      'sm' => true
      ]
    ) ];
  }
}